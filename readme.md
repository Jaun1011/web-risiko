# Risiko Online
## Anforderungen
### Rollen

**Spieler**
- kann neue Einheiten auf Feld setzen
- kann Einheiten auf anderes Feld verschieben
- kann Gegner angreifen mit einheiten
- kann Land besitzen
- kann Land verlieren

- Sieht wer an der Reihe ist
- Sieht übernahmne eines Landes

**Platizerien neuer EInheiten**
- Der Spieler kann auf all seine Felder EInheiten platzieren.
- Auf fremde Felder kann er keine Einheiten sezten
- Der SPieler bekommt pro Feld welcher er besitzt {n} EInheiten.

**Kampf**
- bei einem Angriff gewinnt der mit dem Höheren indikator
- bei einem Gleichstand gewinnt der Verteidiger

**Verschiebung**
- Es muss mindestens eine Einheit auf dem Feld übrig bleiben
- Der Spieler kann nicht mehr einheiten verschieben, als auf dem Feld vorhanden ist
- Wenn keine Einheiten auf dem Feld vorhanden sind geht dies in den Besitz des Spielers ohne Kampf über
- Wenn dem Spieler das Feld bereits gehört, werden die Einheiten verschoben.

## Rest Interface

|Methode| Resource | Beschreibung|
|-------|-------------------|------------------------------------------------|
| POST  | /game             | Erstellt ein neues Spiel mit ausgewählter Map  |
| GET   | /game/{id}        | Gibt Spielinformationen zurück, Wer ist Teil des Spiels wie lange geht es bereits, etc|
| POST  | /game/{id}/player | fügt spieler hinzu |
| GET   | /game/{id}/player | Gibt Spielerinformationen zurück. Welche Felder gehören ihm Welche Einheiten hat er. Gibt das Einheitenkontingent an. Hat Flag ob der Spieler an der Reihe ist|
| GET   | /game/{id}/point/ | Gibt das Punkteneztwerkt zurück|


`PUT /game/{id}/points/`
```json
{
   "player": 2,
   "startPointId": 4,
   "endPointId": 6,
   "enities": 3
}
```
Das Backend macht die Berchnung ob die Route legitim ist, genügent einheiten auf dem Startfeld sind, sowie ob auf dem endPointId fremde Einheiten vorhanden sind

## Daten
*Player*
```json
{
   "playerid": "uuid",
   "controlledPoints": [
       {"id": 1, "entities": 4},
       {"id": 2, "entities": 5},
       {"id": 4, "entities": 3},
       {"id": 6, "entities": 1},
   ],
   "onTurn": false,
   "enitiesTotal": 13,
   "unusedEntities": 3
}
```

*Point*
```json
[
   {
       "id": 1,
       "entities": 4,
       "owner": 1,
       "routes":[
           6
       ],
   },
   {
       "id": 6,
       "entities": 1    
   },
]
```

*Game*
```json
{
   "gameId": 2,
   "createDate": "12.22.1992",
   "turns":[
       {"id": 1, "turn":1, "playerID":4, "players":[{"id":1,...},{"id":2,...}]},
   ]
}
```





## Applikation Builden


```
    $ docker build ./ --build-arg app_env=production
    $ docker run -i -t -p 8080:8080 [image id]
```